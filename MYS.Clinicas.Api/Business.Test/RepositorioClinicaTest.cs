// <copyright file="RepositorioClinicaTest.cs" company="Terr�nea Software">
// Copyright (c) Terr�nea Software. All rights reserved.
// </copyright>

namespace Business.Test
{
    using System.Collections.Generic;
    using AutoMapper;
    using Business.Mock;
    using Business.Repository;
    using Domain.Entities;
    using Domain.Entities.Models;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using Serilog;
    using Xunit;

    /// <summary>
    /// Clase ClinicaRepositoryTest.
    /// </summary>
    public class RepositorioClinicaTest
    {
        private Mock<IMapper> mockMapper = null;
        private Mock<ILogger> mockLogger = null;

        /// <summary>
        /// Comprueba que al solicitar el listado de cl�nicas, �ste no vuelve nulo.
        /// </summary>
        [Fact]
        public void DevuelveClinicasCuandoLlamaBaseDeDatos()
        {
            // Arrange
            this.mockMapper = new Mock<IMapper>();
            this.mockLogger = new Mock<ILogger>();
            DbContextOptions<ClinicasDbContext> optionsBuilder = new DbContextOptionsBuilder<ClinicasDbContext>().UseInMemoryDatabase().Options;
            ClinicasDbContext dbContext = new ClinicasDbContext(optionsBuilder);
            RepositorioClinicas datosClinicas = new RepositorioClinicas(this.mockMapper.Object, dbContext, this.mockLogger.Object);
            IEnumerable<ClinicasModel> clinicasMock = new DataMock().GeneraDatosClinicaModelDBContext();

            foreach (var clinicaMock in clinicasMock)
            {
                dbContext.Add(clinicaMock);
                dbContext.SaveChanges();
            }

            // Act
            IEnumerable<ClinicasModel> clinicas = dbContext.Clinicas;

            // Assert
            Assert.NotNull(clinicas);
        }
    }
}