﻿// <copyright file="ClinicaController.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Api.Models.ErrorModel;
    using Api.Recursos.Constantes;
    using Business.Models.Request;
    using Business.Models.Response;
    using Business.Repository;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Serilog;

    /// <summary>
    /// Controlador clinicas.
    /// </summary>
    [Authorize]
    [Route("api/clinicas")]
    [ApiController]
    public class ClinicaController : ControllerBase
    {
        private readonly IRepositorioClinicas repository;
        private readonly IConfiguration configuration;
        private readonly ILogger logger;
        private string traceId = null;
        private string ipCtx = null;
        private string puertoCtx = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicaController"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="repository">repository.</param>
        /// <param name="configuration">configuration.</param>
        /// <param name="log">serilog.</param>
        public ClinicaController(IRepositorioClinicas repository, IConfiguration configuration, ILogger log)
        {
            this.repository = repository;
            this.configuration = configuration;
            this.logger = log.ForContext<ClinicaController>();
        }

        /// <summary>
        /// Obtiene el listado de todas las clínicas.
        /// </summary>
        /// <returns>Todas las clínicas.</returns>
        [HttpGet]
        public async Task<IActionResult> ObtenerClinicas()
        {
            this.CompruebaCabeceraHttp();

            this.logger.Information(
                "Petición {@" + ConstantesApi.ColTraceId + "} con [ip:puerto][{@" + ConstantesApi.ColIp + "}:{@" + ConstantesApi.ColPuerto + "} solicita el listado de todas las clínicas.",
                this.traceId,
                this.ipCtx,
                this.puertoCtx);

            IEnumerable<Clinica> response = null;
            try
            {
                DatosCabecera contextoHttp = new DatosCabecera()
                {
                    TraceId = this.traceId,
                    IPctx = this.ipCtx,
                    PuertoCtx = this.puertoCtx,
                };

                response = await this.repository.DevuelveClinicas(contextoHttp);
                if (response == null)
                {
                    this.logger.Error(CustomError.ValorNull.Mensaje + "{@" + ConstantesApi.ColTraceId + "} excepción: {" + ConstantesApi.ColExcepcion + "}", this.traceId, new Exception());

                    return this.BadRequest(CustomError.ValorNull);
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(
               "Error al devolver el listado de clínicas en la petición: {" + ConstantesApi.ColTraceId + "} excepción: {" + ConstantesApi.ColExcepcion + "}", this.traceId, ex);

                return this.BadRequest(ex.Message);
            }

            this.logger.Information(
                  "Petición {@" + ConstantesApi.ColTraceId + "} con [ip:puerto][{@" + ConstantesApi.ColIp + "}:{@" + ConstantesApi.ColPuerto + "} obtiene el listado de todas las clínicas.",
                  this.traceId,
                  this.ipCtx,
                  this.puertoCtx);

            return this.Ok(response);
        }

        /// <summary>
        /// Obtiene una sola clinica por el id.
        /// </summary>
        /// <param name="id">Id de la clinica deseada.</param>
        /// <returns>La clinica que tenga ese id.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> ObtenerClinica(string id)
        {
            this.CompruebaCabeceraHttp();

            this.logger.Information(
                "Petición {@" + ConstantesApi.ColTraceId + "} con [ip:puerto][{@" + ConstantesApi.ColIp + "}:{@" + ConstantesApi.ColPuerto + "} solicita el listado de todas las clínicas.",
                this.traceId,
                this.ipCtx,
                this.puertoCtx);

            Clinica response = null;

            try
            {
                DatosCabecera contextoHttp = new DatosCabecera()
                {
                    TraceId = this.traceId,
                    IPctx = this.ipCtx,
                    PuertoCtx = this.puertoCtx,
                };

                response = await this.repository.DevuelveClinica(contextoHttp, id);
                if (response == null)
                {
                    this.logger.Error(CustomError.ValorNull.Mensaje + "{@" + ConstantesApi.ColTraceId + "} excepción: {" + ConstantesApi.ColExcepcion + "}", this.traceId, new Exception());

                    return this.BadRequest(CustomError.ValorNull);
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(
               "Error al devolver la información de la clínica: " + id + " {" + ConstantesApi.ColTraceId + "} excepción: {" + ConstantesApi.ColExcepcion + "}", this.traceId, ex);

                return this.BadRequest(ex.Message);
            }

            this.logger.Information(
                  "Petición {@" + ConstantesApi.ColTraceId + "} con [ip:puerto][{@" + ConstantesApi.ColIp + "}:{@" + ConstantesApi.ColPuerto + "} obtiene los datos de la clínica." + id + " .",
                  this.traceId,
                  this.ipCtx,
                  this.puertoCtx);

            return this.Ok(response);
        }

        /* Métodos auxiliares */

        /// <summary>
        /// Añade el valor original del traceid,ip y puerto de la cabecera http si ésta no es null.
        /// </summary>
        private void CompruebaCabeceraHttp()
        {
            if (this.HttpContext != null)
            {
                this.traceId = this.HttpContext.TraceIdentifier;
                this.ipCtx = this.HttpContext.Connection.RemoteIpAddress.ToString();
                this.puertoCtx = this.HttpContext.Connection.RemotePort.ToString();
            }
        }
    }
}