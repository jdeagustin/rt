﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Business.Repository;
using Domain.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace Api.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
            private readonly IRepositorioAuth repository;
            private readonly IConfiguration configuration;


        public AuthController(IRepositorioAuth repository, IConfiguration configuration)
            {
                this.repository = repository;
                 this.configuration = configuration;
            }

  
            [HttpGet]
            public async Task<IActionResult> Registro(string nombreUsuario, string password)
            {

                UsuarioClinicaModel response = null;
                try
                {


                nombreUsuario = nombreUsuario.ToLower();

                if (await repository.UsuarioExiste(nombreUsuario))
                {
                    return BadRequest("el usuario ya existe");
                }

                UsuarioClinicaModel usuarioNuevo = new UsuarioClinicaModel { Usuario = nombreUsuario };

                response = await this.repository.Registro(usuarioNuevo, password);

                 if (response == null)
                  {
                        return this.BadRequest("No se ha podido crear el usuario");
                  }
                }
                catch (Exception ex)
                {

                    return this.BadRequest(ex.Message);
                }

                return this.Ok("Usuario creado");
            }




        [HttpPost]
        public async Task<IActionResult> Login(string nombreUsuario, string password)
        {

            UsuarioClinicaModel response = null;
            try
            {
                

                nombreUsuario = nombreUsuario.ToLower();

                response = await this.repository.Login(nombreUsuario, password);

                if (response == null)
                {
                    return this.Unauthorized("Credenciales incorrectas.");
                }

            }
            catch (Exception ex)
            {

                return this.BadRequest(ex.Message);
            }

            var claims = new[]
            {
                    new Claim(ClaimTypes.NameIdentifier, nombreUsuario),
                    new Claim(ClaimTypes.Name, nombreUsuario),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration.GetSection("AppSettings:Token").Value));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = credentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);



            return this.Ok(
                new { token = tokenHandler.WriteToken(token) });
        }


    }
}