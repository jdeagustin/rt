﻿// <copyright file="Startup.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using Api.Configuracion;
    using Api.Configuracion.LoggerConfig;
    using Api.Recursos.Constantes;
    using Business.Repository;
    using Domain.Entities;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using Swashbuckle.AspNetCore.Swagger;

    /// <summary>
    /// Clase que determina la configuración de la API.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">configuration.</param>
        /// <param name="environment">enviroment.</param>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            this.Configuration = configuration;
            this.Environment = environment;
        }

        /// <summary>
        /// Devuelve la variable de configuración.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Devuelve la variable de entorno.
        /// </summary>
        public IHostingEnvironment Environment { get; }

        /// <summary>
        /// Método para configurar los servicios.
        /// </summary>
        /// <param name="services">servicio de la api.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<ClinicasDbContext>(
             options => options.UseNpgsql("Host = localhost; Port = 5432; Username = postgres; Password = root; Database = mys;"));
            services.AddSingleton(ConfiguracionAutomapper.AutomapperConfig());
            services.AddSingleton(LogPersonalizado.LogPostgre(this.Configuration.GetConnectionString(this.Environment.EnvironmentName + ConstantesApi.ConexionLog)));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IRepositorioClinicas, RepositorioClinicas>();
            services.AddScoped<IRepositorioAuth, RepositorioAuth>();
            services.AddSingleton<IConfiguration>(this.Configuration);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => 
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                    };
                });


            services.AddCors();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(ConstantesApi.VersionSwagger, new Info
                {
                    Version = ConstantesApi.VersionSwagger,
                    Title = ConstantesApi.TituloSwagger,
                    Description = ConstantesApi.DescripcionSwagger,
                    Contact = new Contact
                    {
                        Name = ConstantesApi.TerraneaSwagger,
                        Email = ConstantesApi.EmailSwagger,
                        Url = ConstantesApi.UrlSwagger,
                    },
                });

                // Configura los comentarios de los métodos para que sean visibles en swagger.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Método para establecer la configuración de la API.
        /// </summary>
        /// <param name="app">app.</param>
        /// <param name="env">env.</param>
        /// <param name="applicationLifetime">applicationLifetime.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(ConstantesApi.SwaggerEndpoint, ConstantesApi.SwaggerEndpointDescripcion);
            });

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();

            /*Logging rendimiento arranque-cierre de la app, el cierre no funciona con IIS Express*/
            applicationLifetime.ApplicationStarted.Register(this.ArrancandoSistema);
            applicationLifetime.ApplicationStopped.Register(this.CerrandoSistema);
        }

        private void ArrancandoSistema() => LogPersonalizado.LogPostgre(this.Configuration.GetConnectionString(this.Environment.EnvironmentName + ConstantesApi.ConexionLog)).Debug(ConstantesApi.AppArrancada);

        private void CerrandoSistema() => LogPersonalizado.LogPostgre(this.Configuration.GetConnectionString(this.Environment.EnvironmentName + ConstantesApi.ConexionLog)).Debug(ConstantesApi.AplicacionParada);

    }
}
