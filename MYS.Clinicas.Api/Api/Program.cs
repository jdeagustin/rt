﻿// <copyright file="Program.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api
{
    using System;
    using System.IO;
    using System.Reflection;
    using Api.Configuracion.LoggerConfig;
    using Api.Recursos.Constantes;
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Clase que arranca la API.
    /// </summary>
    public class Program
    {
        private static string conStr;

        /// <summary>
        /// Clase de arranque de la app.
        /// </summary>
        /// <param name="args"> args.</param>
        public static void Main(string[] args)
        {
            try
            {
                BuildWebHost(args).Run();
            }
            catch (Exception ex)
            {
                LogPersonalizado.LogPostgre(conStr).Error(ConstantesApi.ErroresConfiguracion + "{@" + ConstantesApi.ColExcepcion + "}", ex);
            }
        }

        /// <summary>
        /// WebHost Personalizado.
        /// </summary>
        /// <param name="args">args.</param>
        /// <returns>WebHost.</returns>
        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
                  .ConfigureAppConfiguration(ConfigConfiguration)
                  .UseStartup<Startup>()
                  .Build();

        /// <summary>
        /// Configuración personalizada.
        /// </summary>
        /// <param name="env">Variable de entorno.</param>
        /// <param name="config">Variable de configuración de la app.</param>
        private static void ConfigConfiguration(WebHostBuilderContext env, IConfigurationBuilder config)
        {
            config.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddUserSecrets(assembly: Assembly.Load(new AssemblyName(env.HostingEnvironment.ApplicationName)), optional: true);

            conStr = env.Configuration.GetConnectionString(env.HostingEnvironment.EnvironmentName + ConstantesApi.ConexionLog);

            LogPersonalizado.LogPostgre(conStr).Debug(ConstantesApi.CargandoConfiguracion);
        }
    }
}
