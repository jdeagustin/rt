﻿// <copyright file="ConstantesApi.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api.Recursos.Constantes
{
    /// <summary>
    /// Clase para agregar todos los literales del proyecto.
    /// </summary>
    public static class ConstantesApi
    {
        /*Columnas Logger*/

        /// <summary>
        /// Columna pgsql para el mensaje.
        /// </summary>
        public const string ColMensaje = "mensaje";

        /// <summary>
        /// Columna pgsql para el nivel.
        /// </summary>
        public const string ColNivel = "nivel";

        /// <summary>
        /// Columna pgsql para la fecha.
        /// </summary>
        public const string ColFecha = "fecha";

        /// <summary>
        /// Columna pgsql para la ip.
        /// </summary>
        public const string ColIp = "ip";

        /// <summary>
        /// Columna pgsql para el puerto.
        /// </summary>
        public const string ColPuerto = "puerto";

        /// <summary>
        /// Columna pgsql para las propiedades json.
        /// </summary>
        public const string ColJprop = "jprop";

        /// <summary>
        /// Columna pgsql para el tipo de excepción.
        /// </summary>
        public const string ColExcepcion = "excepcion";

        /// <summary>
        /// Columna pgsql para el trace_id.
        /// </summary>
        public const string ColTraceId = "trace_id";

        /*Logger*/

        /// <summary>
        /// Define el esquema de pgsql para el logger.
        /// </summary>
        public const string EsquemaPublico = "public";

        /// <summary>
        /// Tabla registro de pgsql para el logger.
        /// </summary>
        public const string TablaInfo = "registro";

        /// <summary>
        /// Tabla errores de pgsql para el logger.
        /// </summary>
        public const string TablaErrores = "errores";

        /// <summary>
        /// Tabla rendimiento de pgsql para el logger.
        /// </summary>
        public const string TablaRendimiento = "rendimiento";

        /// <summary>
        /// Literal conexión de pgsql para el logger.
        /// </summary>
        public const string Conexion = "Connection";

        /// <summary>
        /// Literal conexión en el appsettings de pgsql para el logger.
        /// </summary>
        public const string ConexionLog = "LogConnection";

        /// <summary>
        /// Estado de rendimiento para el logger.
        /// </summary>
        public const string AppArrancada = "Aplicación funcionando.";

        /// <summary>
        /// Estado de rendimiento para el logger.
        /// </summary>
        public const string CargandoConfiguracion = "Cargando configuración de la app.";

        /// <summary>
        /// Estado de errores para el logger.
        /// </summary>
        public const string ErroresConfiguracion = "Error al lanzar el archivo de configuración de la app";

        /// <summary>
        /// Estado de rendimiento para el logger.
        /// </summary>
        public const string AplicacionParada = "Aplicación parada.";

        /*Swagger*/

        /// <summary>
        /// Versión de swagger.
        /// </summary>
        public const string VersionSwagger = "v1";

        /// <summary>
        /// Título de la API para swagger.
        /// </summary>
        public const string TituloSwagger = "Mascota y Salud API";

        /// <summary>
        /// Descripción de swagger.
        /// </summary>
        public const string DescripcionSwagger = "API RESTful NetCore 2.2";

        /// <summary>
        /// Nombre de terránea para swagger.
        /// </summary>
        public const string TerraneaSwagger = "Terránea";

        /// <summary>
        /// Email de terránea para swagger.
        /// </summary>
        public const string EmailSwagger = "terranea@terranea.es";

        /// <summary>
        /// URL de Terránea para la documentación de swagger.
        /// </summary>
        public const string UrlSwagger = "https://www.terranea.es/";

        /// <summary>
        /// Endpoint de la configuración de swagger.
        /// </summary>
        public const string SwaggerEndpoint = "/swagger/v1/swagger.json";

        /// <summary>
        /// Descripción del endpoint de swagger.
        /// </summary>
        public const string SwaggerEndpointDescripcion = "Mascota y Salud API RESTful";
    }
}