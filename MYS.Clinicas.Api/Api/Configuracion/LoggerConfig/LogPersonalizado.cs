﻿// <copyright file="LogPersonalizado.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api.Configuracion.LoggerConfig
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Api.Recursos.Constantes;
    using NpgsqlTypes;
    using Serilog;
    using Serilog.Events;
    using Serilog.Sinks.PostgreSQL;

    /// <summary>
    /// Define la parametrización del logger personalizado para la API.
    /// </summary>
    public static class LogPersonalizado
    {
        private static ILogger log;

        /// <summary>
        /// Logger por defecto a postgresql.
        /// </summary>
        /// <param name="connectionstring">Conexión a bbdd para depositar los logs.</param>
        /// <returns>Logger configurado para actuar contra postgre.</returns>
        public static ILogger LogPostgre(string connectionstring)
        {
            IDictionary<string, ColumnWriterBase> columnasRendimiento = new Dictionary<string, ColumnWriterBase>
        {
            { ConstantesApi.ColMensaje, new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
            { ConstantesApi.ColNivel, new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
            { ConstantesApi.ColFecha, new TimestampColumnWriter(NpgsqlDbType.TimestampTz) },
            { ConstantesApi.ColJprop, new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
        };

            IDictionary<string, ColumnWriterBase> columnasRegistro = new Dictionary<string, ColumnWriterBase>
        {
            { ConstantesApi.ColMensaje, new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
            { ConstantesApi.ColTraceId, new SinglePropertyColumnWriter(ConstantesApi.ColTraceId, PropertyWriteMethod.ToString, NpgsqlDbType.Text) },
            { ConstantesApi.ColNivel, new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
            { ConstantesApi.ColFecha, new TimestampColumnWriter(NpgsqlDbType.TimestampTz) },
            { ConstantesApi.ColIp, new SinglePropertyColumnWriter(ConstantesApi.ColIp, PropertyWriteMethod.ToString, NpgsqlDbType.Varchar) },
            { ConstantesApi.ColPuerto, new SinglePropertyColumnWriter(ConstantesApi.ColPuerto, PropertyWriteMethod.ToString, NpgsqlDbType.Varchar) },
            { ConstantesApi.ColJprop, new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
        };

            IDictionary<string, ColumnWriterBase> columnasErrores = new Dictionary<string, ColumnWriterBase>
        {
            { ConstantesApi.ColMensaje, new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
            { ConstantesApi.ColTraceId, new SinglePropertyColumnWriter(ConstantesApi.ColTraceId, PropertyWriteMethod.ToString, NpgsqlDbType.Text) },
            { ConstantesApi.ColNivel, new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
            { ConstantesApi.ColFecha, new TimestampColumnWriter(NpgsqlDbType.TimestampTz) },
            { ConstantesApi.ColExcepcion, new SinglePropertyColumnWriter(ConstantesApi.ColExcepcion, PropertyWriteMethod.Raw, NpgsqlDbType.Text) },
            { ConstantesApi.ColJprop, new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
        };

            log = new LoggerConfiguration()
              .MinimumLevel.Debug()
              .Enrich.FromLogContext()
              .WriteTo.Logger(ld => ld
                .Filter.ByIncludingOnly(z => z.Level == LogEventLevel.Debug)
                    .WriteTo.PostgreSQL(connectionstring, ConstantesApi.TablaRendimiento, columnasRendimiento, LogEventLevel.Debug, null, CultureInfo.InvariantCulture, 30, null, true, ConstantesApi.EsquemaPublico, false))
              .WriteTo.Logger(lc => lc
                .Filter.ByIncludingOnly(z => z.Level == LogEventLevel.Information)
              .WriteTo.PostgreSQL(connectionstring, ConstantesApi.TablaInfo, columnasRegistro, LogEventLevel.Information, null, CultureInfo.InvariantCulture, 30, null, true, ConstantesApi.EsquemaPublico, false))

          .WriteTo.Logger(le => le
             .Filter.ByIncludingOnly(z => z.Level == LogEventLevel.Error)
             .WriteTo.PostgreSQL(connectionstring, ConstantesApi.TablaErrores, columnasErrores, LogEventLevel.Error, null, CultureInfo.InvariantCulture, 30, null, true, ConstantesApi.EsquemaPublico, false))
          .CreateLogger();

            AppDomain.CurrentDomain.ProcessExit += (s, e) => Serilog.Log.CloseAndFlush();

            return log;
        }
    }
}