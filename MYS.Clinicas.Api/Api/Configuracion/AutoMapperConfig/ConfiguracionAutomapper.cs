﻿// <copyright file="ConfiguracionAutomapper.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>
namespace Api.Configuracion
{
    using AutoMapper;
    using Business.Perfiles;

    /// <summary>
    /// Clase estática para configurar Automapper.
    /// </summary>
    public static class ConfiguracionAutomapper
    {
        /// <summary>
        /// Método estático que añade el servicio automapper mediante instancia en Startup.cs .
        /// </summary>
        /// <returns>Imapper.</returns>
        public static IMapper AutomapperConfig()
        {
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new PerfilClinica());
            });

            return mappingConfig.CreateMapper();
        }
    }
}
