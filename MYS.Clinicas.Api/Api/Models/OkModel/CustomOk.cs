﻿// <copyright file="CustomOk.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api.Models.OkModel
{
    /// <summary>
    /// Clase para personalizar los tipos de respuestas correctas en la response.
    /// </summary>
    public class CustomOk
    {
        /// <summary>
        /// Response OK al crear una clínica.
        /// </summary>
        public static CustomOk ClinicaCreada { get; } = new CustomOk()
        {
            Codigo = 2001,
            Mensaje = "La clínica se ha añadido correctamente",
        };

        /// <summary>
        /// Response OK al actualizar una clínica.
        /// </summary>
        public static CustomOk ClinicaActualizada { get; } = new CustomOk()
        {
            Codigo = 2002,
            Mensaje = "La clínica se ha actualizado correctamente",
        };

        /// <summary>
        /// Response OK al eliminar una clínica.
        /// </summary>
        public static CustomOk ClinicaEliminada { get; } = new CustomOk()
        {
            Codigo = 2003,
            Mensaje = "La clínica se ha eliminado correctamente",
        };

        /// <summary>
        /// Gets Codigo.
        /// </summary>
        public int Codigo { get; private set; }

        /// <summary>
        /// Gets Mensaje.
        /// </summary>
        public string Mensaje { get; private set; }
    }
}
