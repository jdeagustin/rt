﻿// <copyright file="CustomError.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Api.Models.ErrorModel
{
    /// <summary>
    /// Clase para personalizar los tipos de error en la response.
    /// </summary>
    public class CustomError
    {
        /// <summary>
        /// Response errónea al crear una clínica.
        /// </summary>
        public static CustomError ValorNull { get; } = new CustomError()
        {
            Codigo = 4001,
            Mensaje = "Objeto nulo",
        };

        /// <summary>
        /// Error en operaciones SQL.
        /// </summary>
        public static CustomError ErrorSql { get; } = new CustomError()
        {
            Codigo = 5001,
            Mensaje = "Error en el acceso a la base de datos",
        };

        /// <summary>
        /// Gets Codigo.
        /// </summary>
        public int Codigo { get; private set; }

        /// <summary>
        /// Gets Mensaje.
        /// </summary>
        public string Mensaje { get; private set; }
    }
}
