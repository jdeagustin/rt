﻿// <copyright file="RepositorioClinicas.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Api.Recursos.Constantes;
    using AutoMapper;
    using Business.Models.Request;
    using Business.Models.Response;
    using Domain.Entities;
    using Domain.Entities.Models;
    using Microsoft.EntityFrameworkCore;
    using Serilog;

    /// <summary>
    /// Business Layer Clinicas.
    /// </summary>
    public class RepositorioClinicas : IRepositorioClinicas
    {
        private readonly IMapper mapper;
        private readonly ClinicasDbContext ctx;
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositorioClinicas"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="mapper">mapeo de modelos E/S.</param>
        /// <param name="ctx">contexto bbdd.</param>
        /// <param name="log">log.</param>
        public RepositorioClinicas(IMapper mapper, ClinicasDbContext ctx, ILogger log)
        {
            this.mapper = mapper;
            this.ctx = ctx;
            this.logger = log.ForContext<RepositorioClinicas>();
        }

        /// <summary>
        /// Devuelve todas las clínicas de la base de datos.
        /// </summary>
        /// <returns>Todas las clínicas de la tabla Clínicas.</returns>
        public async Task<IEnumerable<Clinica>> DevuelveClinicas(DatosCabecera contextoHttp)
        {

           this.logger.Information(
           "Petición {@" + ConstantesBL.ColTraceId + "} con [ip:puerto][{@" + ConstantesBL.ColIp + "}:{@" + ConstantesBL.ColPuerto + "} accediendo a los datos del map EF-Core.",
           contextoHttp.TraceId,
           contextoHttp.IPctx,
           contextoHttp.PuertoCtx);

           IEnumerable<Clinica> modelClinica = this.mapper.Map<IEnumerable<Clinica>>(this.ctx.Clinicas);

           this.logger.Information(
          "Petición {@" + ConstantesBL.ColTraceId + "} con [ip:puerto][{@" + ConstantesBL.ColIp + "}:{@" + ConstantesBL.ColPuerto + "} añadiendo el modelo Especialidades a cada clínica.",
          contextoHttp.TraceId,
          contextoHttp.IPctx,
          contextoHttp.PuertoCtx);

           foreach (var clinica in modelClinica)
           {
               clinica.Especialidades = this.mapper.Map<IEnumerable<Especialidad>>(this.ctx.EspecialidadesPorClinicas.Where(x => x.IdClinica == clinica.Id).Select(y => y.Especialidades));
           }

           this.logger.Information(
          "Petición {@" + ConstantesBL.ColTraceId + "} con [ip:puerto][{@" + ConstantesBL.ColIp + "}:{@" + ConstantesBL.ColPuerto + "} Modelo de datos obtenido.",
          contextoHttp.TraceId,
          contextoHttp.IPctx,
          contextoHttp.PuertoCtx);

           return modelClinica;
        }

        /// <summary>
        /// Devuelve la clinica que coincida con el id.
        /// </summary>
        /// <param name="contextoHttp">contexto Http del controlador.</param>
        /// <param name="id">Id de la clinica deseada.</param>
        /// <returns>Devuelve una sola clinica de la base de datos.</returns>
        public async Task<Clinica> DevuelveClinica(DatosCabecera contextoHttp, string id)
        {
            this.logger.Information(
            "Petición {@" + ConstantesBL.ColTraceId + "} con [ip:puerto][{@" + ConstantesBL.ColIp + "}:{@" + ConstantesBL.ColPuerto + "} accediendo a los datos del map EF-Core.",
            contextoHttp.TraceId,
            contextoHttp.IPctx,
            contextoHttp.PuertoCtx);

            Clinica clinica = this.mapper.Map<Clinica>(this.ctx.Clinicas.Where(x => x.IdClinica.Equals(id)).FirstOrDefault());

            this.logger.Information(
           "Petición {@" + ConstantesBL.ColTraceId + "} con [ip:puerto][{@" + ConstantesBL.ColIp + "}:{@" + ConstantesBL.ColPuerto + "} Clinica obtenida.",
           contextoHttp.TraceId,
           contextoHttp.IPctx,
           contextoHttp.PuertoCtx);

            return clinica;
        }
    }
}
