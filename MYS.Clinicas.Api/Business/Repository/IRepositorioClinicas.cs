﻿// <copyright file="IRepositorioClinicas.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Business.Models.Request;
    using Business.Models.Response;

    /// <summary>
    /// Interfaz IClinicRepository.
    /// </summary>
    public interface IRepositorioClinicas
    {
      /// <summary>
      /// Método para obtener todas las clínicas del map EF-Core.
      /// </summary>
      /// <param name="contextoHttp">Contexto Http del controlador.</param>
      /// <returns>Listado de clínicas.</returns>
        Task<IEnumerable<Clinica>> DevuelveClinicas(DatosCabecera contextoHttp);

        /// <summary>
        /// Metodo que obtiene una sola clinica parametrada por el id.
        /// </summary>
        /// <param name="contextoHttp">contexto Http del controlador.</param>
        /// <param name="id">Id de la clinica deseada.</param>
        /// <returns>Clinica seleccionada.</returns>
        Task<Clinica> DevuelveClinica(DatosCabecera contextoHttp, string id);
    }
}
