﻿// <copyright file="RepositorioClinicas.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Api.Recursos.Constantes;
    using AutoMapper;
    using Business.Models.Request;
    using Business.Models.Response;
    using Domain.Entities;
    using Domain.Entities.Models;
    using Microsoft.EntityFrameworkCore;
    using Serilog;

    /// <summary>
    /// Business Layer Clinicas.
    /// </summary>
    public class RepositorioAuth : IRepositorioAuth
    {
        private readonly IMapper mapper;
        private readonly ClinicasDbContext ctx;
        private readonly ILogger logger;


        public RepositorioAuth(IMapper mapper, ClinicasDbContext ctx, ILogger log)
        {
            this.mapper = mapper;
            this.ctx = ctx;
            this.logger = log.ForContext<RepositorioAuth>();
        }

        public async Task<UsuarioClinicaModel> Login(string nombreUsuario, string password)
        {
            UsuarioClinicaModel usuario = this.ctx.Usuarios.Where(x=> x.Usuario == nombreUsuario).FirstOrDefault();

            if (usuario == null)
            {
                return null;
            }

            if (!this.ComprobarHashpassword(password, usuario.PasswordHash, usuario.PasswordSalt))
            {
                return null;
            }

            return usuario;
        }

        public async Task<UsuarioClinicaModel> Registro(UsuarioClinicaModel usuario, string password)
        {
            byte[] passwordHash, passwordSalt;
            this.GeneradorPassword(password, out passwordHash, out passwordSalt);

            usuario.PasswordHash = passwordHash;
            usuario.PasswordSalt = passwordSalt;

            this.ctx.Usuarios.Add(usuario);
            this.ctx.SaveChanges();

            return usuario;
        }


        public async Task<bool> UsuarioExiste(string nombreUsuario)
        {

            if (await this.ctx.Usuarios.AnyAsync(x => x.Usuario == nombreUsuario))
            {
                return true;
            }
            else
            {
                return false;
            }

        }



        //Generador de passwords
        private void GeneradorPassword(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            HMACSHA512 hmac = new HMACSHA512();
            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
        }

        //Comprueba hash
        private bool ComprobarHashpassword(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            HMACSHA512 hmac = new HMACSHA512(passwordSalt);

            
            var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

            for (int i=0; i<computedHash.Length; i++)
            {
                if (computedHash[i] != passwordHash[i])
                {
                    return false;
                }
            }

            return true;

        }

    }
}
