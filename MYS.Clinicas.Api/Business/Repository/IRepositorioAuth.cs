﻿
namespace Business.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Business.Models.Request;
    using Business.Models.Response;
    using Domain.Entities.Models;

    public interface IRepositorioAuth
    {



        Task<UsuarioClinicaModel> Registro(UsuarioClinicaModel usuario, string password);

        Task<UsuarioClinicaModel> Login(string nombreUsuario, string password);

        Task<bool> UsuarioExiste(string nombreUsuario);
    }
}
