﻿// <copyright file="DataMock.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Mock
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Business.Models.Response;
    using Domain.Entities.Models;

    /// <summary>Clase para cargar datos hardcodeados.</summary>
    public class DataMock
    {
        /// <summary>
        /// Genera clínicas mock.
        /// </summary>
        /// <param name="isNull">isNull.</param>
        /// <returns>Lista de clínicas mock.</returns>
        public async Task<IEnumerable<Clinica>> GeneraDatosClinicaAsync(bool isNull = false)
        {
            List<Clinica> clinicasList = new List<Clinica>
            {
                new Clinica
                {
                    CodigoProvincia = "22",
                    CP = "28801",
                    Direccion = "Calle Perro",
                    Id = "1",
                    Latitud = 11.1f,
                    Longitud = 12.1f,
                    Nombre = "Calle de los perros 2",
                    NombreEspecialista = "Juan Carlos",
                    Poblacion = "Madrid",
                    Provincia = "Madrid",
                    Telefono = "918034563",
                    TelefonoPublico = 122,
                    TipoCentro = "Clinica",
                },
                new Clinica
                {
                    CodigoProvincia = "11",
                    CP = "28805",
                    Direccion = "Calle Gato",
                    Id = "2",
                    Latitud = 11.1f,
                    Longitud = 12.1f,
                    Nombre = "Calle de los gatos 5",
                    NombreEspecialista = "María Fernandez",
                    Poblacion = "Madrid",
                    Provincia = "Madrid",
                    Telefono = "918034563",
                    TelefonoPublico = 122,
                    TipoCentro = "Clinica",
                },
                new Clinica
                {
                    CodigoProvincia = "96",
                    CP = "28900",
                    Direccion = "Calle Pájaro",
                    Id = "3",
                    Latitud = 11.1f,
                    Longitud = 12.1f,
                    Nombre = "Calle de los pájaros 2",
                    NombreEspecialista = "Juana",
                    Poblacion = "Madrid",
                    Provincia = "Madrid",
                    Telefono = "918034563",
                    TelefonoPublico = 122,
                    TipoCentro = "Clinica",
                },
            };

            IEnumerable<Clinica> clinicas = isNull ? null : clinicasList;

            return clinicas;
        }

        /// <summary>
        /// Genera Clínicas para el el mock de base de datos.
        /// </summary>
        /// <returns>Listado de clínicas mockDB.</returns>
        public IEnumerable<ClinicasModel> GeneraDatosClinicaModelDBContext()
        {
            return new List<ClinicasModel>
            {
                new ClinicasModel
                {
                    CodigoProvincia = 22,
                    CP = "28801",
                    Direccion = "Calle Perro",
                    IdClinica = "1",
                    Latitud = 11.1f,
                    Longitud = 12.1f,
                    NombreCentro = "Calle de los perros 2",
                    NombreEspecialista = "Juan Carlos",
                    Poblacion = "Madrid",
                    Provincia = "Madrid",
                    Telefono = "918034563",
                    TelefonoPublico = 122,
                    TipoCentro = "Clinica",
                },
                new ClinicasModel
                {
                    CodigoProvincia = 11,
                    CP = "28805",
                    Direccion = "Calle Gato",
                    IdClinica = "2",
                    Latitud = 11.1f,
                    Longitud = 12.1f,
                    NombreCentro = "Calle de los gatos 5",
                    NombreEspecialista = "María Fernandez",
                    Poblacion = "Madrid",
                    Provincia = "Madrid",
                    Telefono = "918034563",
                    TelefonoPublico = 122,
                    TipoCentro = "Clinica",
                },
                new ClinicasModel
                {
                    CodigoProvincia = 96,
                    CP = "28900",
                    Direccion = "Calle Pájaro",
                    IdClinica = "3",
                    Latitud = 11.1f,
                    Longitud = 12.1f,
                    NombreCentro = "Calle de los pájaros 2",
                    NombreEspecialista = "Juana",
                    Poblacion = "Madrid",
                    Provincia = "Madrid",
                    Telefono = "918034563",
                    TelefonoPublico = 122,
                    TipoCentro = "Clinica",
                },
            };
        }
    }
}
