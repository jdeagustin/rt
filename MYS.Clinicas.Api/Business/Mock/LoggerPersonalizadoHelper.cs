﻿// <copyright file="LoggerPersonalizadoHelper.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Mock
{
    using Serilog;

    /// <summary>
    /// Log mock para test.
    /// </summary>
    public class LoggerPersonalizadoHelper
    {
        private ILogger log;

        /// <summary>
        /// Configura log mockeado.
        /// </summary>
        /// <returns>LogMock.</returns>
        public ILogger LogSilencioso()
        {
            return this.log = new LoggerConfiguration().WriteTo.Logger(Serilog.Log.Logger).CreateLogger();
        }
    }
}
