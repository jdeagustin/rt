﻿// <copyright file="PerfilClinica.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Perfiles
{
    using Api.Recursos.Constantes;
    using AutoMapper;
    using Business.Models.Response;
    using Domain.Entities.Models;

    /// <summary>
    /// Clase para el mapeo de objetos tipo clínica.
    /// </summary>
    public class PerfilClinica : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PerfilClinica"/> class.
        /// </summary>
        public PerfilClinica()
        {
            /*Destino --> Origen*/

            this.MapFromClinicaModelToClinica();
            this.MapFromClinicaToClinicaModel();

            this.MapFromEspecialidadesPorCentrosModelToEspecialidadesPorCentros();
            this.MapFromEspecialidadesPorCentrosToEspecialidadesPorCentrosModel();

            this.MapFromEspecialidadesModelToEspecialidades();
            this.MapFromEspecialidadesToEspecialidadesModel();
        }

        private void MapFromClinicaModelToClinica()
        {
            this.CreateMap<ClinicasModel, Clinica>()
            .ForMember(dest => dest.Id, orig => orig.MapFrom(prop => prop.IdClinica))
            .ForMember(dest => dest.CodigoProvincia, orig => orig.MapFrom(prop => prop.CodigoProvincia))
            .ForMember(dest => dest.CP, orig => orig.MapFrom(prop => prop.CP))
            .ForMember(dest => dest.Direccion, orig => orig.MapFrom(prop => prop.Direccion))
            .ForMember(dest => dest.Latitud, orig => orig.MapFrom(prop => prop.Latitud))
            .ForMember(dest => dest.Longitud, orig => orig.MapFrom(prop => prop.Longitud))
            .ForMember(dest => dest.Nombre, orig => orig.MapFrom(prop => prop.NombreCentro))
            .ForMember(dest => dest.NombreEspecialista, orig => orig.MapFrom(prop => prop.NombreEspecialista))
            .ForMember(dest => dest.Poblacion, orig => orig.MapFrom(prop => prop.Poblacion))
            .ForMember(dest => dest.Provincia, orig => orig.MapFrom(prop => prop.Provincia))
            .ForMember(dest => dest.Telefono, orig => orig.MapFrom(prop => prop.Telefono))
            .ForMember(dest => dest.TelefonoPublico, orig => orig.MapFrom(prop => prop.TelefonoPublico))
            .ForMember(dest => dest.TipoCentro, orig => orig.MapFrom(prop => prop.TipoCentro));
        }

        private void MapFromClinicaToClinicaModel()
        {
            this.CreateMap<Clinica, ClinicasModel>()
            .ForMember(dest => dest.IdClinica, orig => orig.MapFrom(prop => prop.Id))
            .ForMember(dest => dest.CodigoProvincia, orig => orig.MapFrom(prop => int.Parse(prop.CodigoProvincia)))
            .ForMember(dest => dest.CP, orig => orig.MapFrom(prop => prop.CP))
            .ForMember(dest => dest.Direccion, orig => orig.MapFrom(prop => prop.Direccion))
            .ForMember(dest => dest.Latitud, orig => orig.MapFrom(prop => prop.Latitud))
            .ForMember(dest => dest.Longitud, orig => orig.MapFrom(prop => prop.Longitud))
            .ForMember(dest => dest.NombreCentro, orig => orig.MapFrom(prop => prop.Nombre))
            .ForMember(dest => dest.NombreEspecialista, orig => orig.MapFrom(prop => prop.NombreEspecialista))
            .ForMember(dest => dest.Poblacion, orig => orig.MapFrom(prop => prop.Poblacion))
            .ForMember(dest => dest.Provincia, orig => orig.MapFrom(prop => prop.Provincia))
            .ForMember(dest => dest.Telefono, orig => orig.MapFrom(prop => prop.Telefono))
            .ForMember(dest => dest.TelefonoPublico, orig => orig.MapFrom(prop => prop.TelefonoPublico))
            .ForMember(dest => dest.TipoCentro, orig => orig.MapFrom(prop => prop.TipoCentro));
        }

        private void MapFromEspecialidadesPorCentrosModelToEspecialidadesPorCentros()
        {
            this.CreateMap<EspecialidadesPorCentroModel, EspecialidadPorCentro>()
            .ForMember(dest => dest.IdClinica, orig => orig.MapFrom(prop => prop.IdClinica))
            .ForMember(dest => dest.IdEspecialidad, orig => orig.MapFrom(prop => prop.IdEspecialidad));
        }

        private void MapFromEspecialidadesPorCentrosToEspecialidadesPorCentrosModel()
        {
            this.CreateMap<EspecialidadPorCentro, EspecialidadesPorCentroModel>()
            .ForMember(dest => dest.IdClinica, orig => orig.MapFrom(prop => prop.IdClinica))
            .ForMember(dest => dest.IdEspecialidad, orig => orig.MapFrom(prop => prop.IdEspecialidad));
        }

        private void MapFromEspecialidadesModelToEspecialidades()
        {
            this.CreateMap<EspecialidadesModel, Especialidad>()
            .ForMember(dest => dest.Id, orig => orig.MapFrom(prop => prop.IdEspecialidad))
            .ForMember(dest => dest.Nombre, orig => orig.MapFrom(prop => prop.Especialidad))
            .ForMember(dest => dest.Otros, orig => orig.MapFrom(prop => prop.Otros ?? ConstantesBL.SinDatos));
        }

        private void MapFromEspecialidadesToEspecialidadesModel()
        {
            this.CreateMap<Especialidad, EspecialidadesModel>()
            .ForMember(dest => dest.IdEspecialidad, orig => orig.MapFrom(prop => prop.Id))
            .ForMember(dest => dest.Especialidad, orig => orig.MapFrom(prop => prop.Nombre))
            .ForMember(dest => dest.Otros, orig => orig.MapFrom(prop => prop.Otros));
        }
    }
}
