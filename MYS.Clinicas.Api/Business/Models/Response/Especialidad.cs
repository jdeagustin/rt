﻿// <copyright file="Especialidades.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Models.Response
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Clase Especialidades.
    /// </summary>
    public class Especialidad
    {
        /// <summary>
        /// Gets or Sets IdEspecialidad.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets Especialidad.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or Sets Otros.
        /// </summary>
        public string Otros { get; set; }
    }
}
