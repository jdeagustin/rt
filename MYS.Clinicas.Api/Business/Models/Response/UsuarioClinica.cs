﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models.Response
{
    public class UsuarioClinica
    {

        public int Id {get; set;}

        public string Usuario { get; set; }

        public byte [] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

    }
}
