﻿// <copyright file="Clinica.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Models.Response
{
    using System.Collections.Generic;

    /// <summary>
    /// Clase Clínica.
    /// </summary>
    public class Clinica
    {
        /// <summary>
        /// Gets or Sets IdClinica.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets NombreCentro.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or Sets TipoCentro.
        /// </summary>
        public string TipoCentro { get; set; }

        /// <summary>
        /// Gets or Sets NombreEspecialista.
        /// </summary>
        public string NombreEspecialista { get; set; }

        /// <summary>
        /// Gets or Sets CodigoProvincia.
        /// </summary>
        public string CodigoProvincia { get; set; }

        /// <summary>
        /// Gets or Sets Provincia.
        /// </summary>
        public string Provincia { get; set; }

        /// <summary>
        /// Gets or Sets Poblacion.
        /// </summary>
        public string Poblacion { get; set; }

        /// <summary>
        /// Gets or Sets Telefono.
        /// </summary>
        public string Telefono { get; set; }

        /// <summary>
        /// Gets or Sets TelefonoPublico.
        /// </summary>
        public byte TelefonoPublico { get; set; }

        /// <summary>
        /// Gets or Sets CP.
        /// </summary>
        public string CP { get; set; }

        /// <summary>
        /// Gets or Sets Direccion.
        /// </summary>
        public string Direccion { get; set; }

        /// <summary>
        /// Gets or Sets Latitud.
        /// </summary>
        public double Latitud { get; set; }

        /// <summary>
        /// Gets or Sets Longitud.
        /// </summary>
        public double Longitud { get; set; }

        /// <summary>
        /// Gets or sets especialidades.
        /// </summary>
        public IEnumerable<Especialidad> Especialidades { get; set; }
    }
}