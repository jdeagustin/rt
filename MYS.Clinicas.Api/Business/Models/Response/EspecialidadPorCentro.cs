﻿// <copyright file="EspecialidadesPorCentros.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Models.Response
{
    /// <summary>
    /// Clase EspecialidadesPorCentros.
    /// </summary>
    public class EspecialidadPorCentro
    {
        /// <summary>
        /// Gets or Sets IdClinica.
        /// </summary>
        public string IdClinica { get; set; }

        /// <summary>
        /// Gets or Sets IdEspecialidad.
        /// </summary>
        public int IdEspecialidad { get; set; }
    }
}