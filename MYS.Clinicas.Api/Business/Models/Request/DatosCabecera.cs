﻿// <copyright file="DatosCabecera.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Business.Models.Request
{
    /// <summary>
    /// Clase para modelar los datos del contexto http para serilog.
    /// </summary>
    public class DatosCabecera
    {
        /// <summary>
        /// Traza del contexto Http.
        /// </summary>
        public string TraceId { get ; set; }

        /// <summary>
        /// Ip cliente.
        /// </summary>
        public string IPctx { get; set; }

        /// <summary>
        /// Puerto cliente.
        /// </summary>
        public string PuertoCtx { get; set; }
    }
}
