﻿// <copyright file="ConstantesBL.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>
namespace Api.Recursos.Constantes
{
    /// <summary>
    /// Constantes para el proyecto BL.
    /// </summary>
    public static class ConstantesBL
    {
        /// <summary>
        /// Literal para indicar que no existen datos para ese registro.
        /// </summary>
        public static string SinDatos = "Sin datos";

        /// <summary>
        /// Columna pgsql para el trace_id.
        /// </summary>
        public const string ColTraceId = "trace_id";

        /// <summary>
        /// Columna pgsql para la ip.
        /// </summary>
        public const string ColIp = "ip";

        /// <summary>
        /// Columna pgsql para el puerto.
        /// </summary>
        public const string ColPuerto = "puerto";
    }
}