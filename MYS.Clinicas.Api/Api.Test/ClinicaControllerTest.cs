// <copyright file="ClinicaControllerTest.cs" company="Terr�nea Software">
// Copyright (c) Terr�nea Software. All rights reserved.
// </copyright>
namespace Api.Test
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Api.Controllers;
    using Business.Mock;
    using Business.Models.Request;
    using Business.Models.Response;
    using Business.Repository;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Moq;
    using Xunit;

    /// <summary>
    /// Clase que se encarca de testear los controladores de la API.
    /// </summary>
    public class ClinicaControllerTest
    {
        private Mock<IRepositorioClinicas> mockRepo = null;
        private Mock<LoggerPersonalizadoHelper> mockLogger = null;
        private Mock<IConfiguration> mockConfig = null;
        private ClinicaController controller = null;
        private Task<IActionResult> response = null;

        /// <summary>
        /// Comprueba que la respuesta es correcta cuando devuelve la lista de todas las cl�nicas de la base de datos.
        /// </summary>
        [Fact]
        public async void OkCuandoDevuelveClinicas()
        {
            // Arrange
            this.mockRepo = new Mock<IRepositorioClinicas>();
            IEnumerable<Clinica> clinicasMock = await new DataMock().GeneraDatosClinicaAsync();
            this.mockRepo.Setup(x => x.DevuelveClinicas(It.IsAny<DatosCabecera>())).ReturnsAsync(clinicasMock);
            this.mockConfig = new Mock<IConfiguration>();
            this.mockLogger = new Mock<LoggerPersonalizadoHelper>();

            this.controller = new ClinicaController(this.mockRepo.Object, this.mockConfig.Object, this.mockLogger.Object.LogSilencioso());

            this.response = this.controller.ObtenerClinicas();

            // Assert
            Assert.IsType<OkObjectResult>(this.response.Result);
        }

        /// <summary>
        /// Comprueba que la respuesta es un bad request cuando el resultado es nulo.
        /// </summary>
        [Fact]
        public async void BadRequestCuandoEsNull()
        {
            // Arrange
            this.mockRepo = new Mock<IRepositorioClinicas>();
            IEnumerable<Clinica> clinicasMock = await new DataMock().GeneraDatosClinicaAsync(true);
            this.mockRepo.Setup(x => x.DevuelveClinicas(It.IsAny<DatosCabecera>())).ReturnsAsync(clinicasMock);
            this.mockConfig = new Mock<IConfiguration>();
            this.mockLogger = new Mock<LoggerPersonalizadoHelper>();

            this.controller = new ClinicaController(this.mockRepo.Object, this.mockConfig.Object, this.mockLogger.Object.LogSilencioso());

            this.response = this.controller.ObtenerClinicas();

            // Assert
            Assert.IsType<BadRequestObjectResult>(this.response.Result);
        }
    }
}
