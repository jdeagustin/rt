﻿// <copyright file="DatabaseContext.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Domain.Entities
{
    using Domain.Entities.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Clase que configura el contexto de la base de datos.
    /// </summary>
    public class ClinicasDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicasDbContext"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="options">options.</param>
        public ClinicasDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Tabla Clínicas.
        /// </summary>
        public DbSet<ClinicasModel> Clinicas { get; set; }

        ///// <summary>
        ///// Tabla especialidades.
        ///// </summary>
        public DbSet<EspecialidadesModel> Especialidades { get; set; }

        ///// <summary>
        ///// Tabla especialidades por clínicas.
        ///// </summary>
        public DbSet<EspecialidadesPorCentroModel> EspecialidadesPorClinicas { get; set; }

        public DbSet<UsuarioClinicaModel> Usuarios { get; set; }

        /// <summary>
        /// Método para poder configurar el fluent api.
        /// </summary>
        /// <param name="builder">builder.</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            /* Tabla EspecialidadesPorCentros__FluentAPI__*/
            builder.Entity<EspecialidadesPorCentroModel>().HasKey(s => new { s.IdClinica,s.IdEspecialidad});
        }
    }
}
