﻿// <copyright file="EspecialidadesModel.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Domain.Entities.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Define la tabla Especialidades.
    /// </summary>
    public class EspecialidadesModel
    {
        /// <summary>
        /// Gets or Sets IdEspecialidad.
        /// </summary>
        [Key]
        public int IdEspecialidad { get; set; }

        /// <summary>
        /// Gets or Sets Especialidad.
        /// </summary>
        public string Especialidad { get; set; }

        /// <summary>
        /// Gets or Sets Otros.
        /// </summary>
        public string Otros { get; set; }

        /// <summary>
        /// Gets or Sets EspecialidadesPorCEntros.
        /// </summary>
        public List<EspecialidadesPorCentroModel> EspecialidadesPorCentros { get; set; }
    }
}
