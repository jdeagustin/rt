﻿// <copyright file="EspecialidadesPorCentrosModel.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Domain.Entities.Models
{
    /// <summary>
    /// Define la tabla EspecialidadesPorCentros.
    /// </summary>
    /// 
    using System.Linq;

    public class EspecialidadesPorCentroModel
    {
        /// <summary>
        /// Gets or Sets IdClinica.
        /// </summary>
        public string IdClinica { get; set; }

        /// <summary>
        /// Gets or Sets IdEspecialidad.
        /// </summary>
        public int IdEspecialidad { get; set; }

        /// <summary>
        /// Gets or Sets Especialidades.
        /// </summary>
        public EspecialidadesModel Especialidades { get; set; }

        /// <summary>
        /// Gets or Sets Clinicas.
        /// </summary>
        public ClinicasModel Clinicas { get; set; }

        /// <summary>
        /// Gets or sets numCambios.
        /// </summary>
        public int NumCambios { get; set; }
    }
}