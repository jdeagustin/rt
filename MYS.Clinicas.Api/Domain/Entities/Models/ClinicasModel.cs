﻿// <copyright file="ClinicaModel.cs" company="Terránea Software">
// Copyright (c) Terránea Software. All rights reserved.
// </copyright>

namespace Domain.Entities.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Define la tabla Clínicas.
    /// </summary>
    public class ClinicasModel
    {
        /// <summary>
        /// Gets or Sets IdClinica.
        /// </summary>
        [Key]
        public string IdClinica { get; set; }

        /// <summary>
        /// Gets or Sets NombreCentro.
        /// </summary>
        public string NombreCentro { get; set; }

        /// <summary>
        /// Gets or Sets TipoCentro.
        /// </summary>
        public string TipoCentro { get; set; }

        /// <summary>
        /// Gets or Sets NombreEspecialista.
        /// </summary>
        public string NombreEspecialista { get; set; }

        /// <summary>
        /// Gets or Sets CodigoProvincia.
        /// </summary>
        public int CodigoProvincia { get; set; }

        /// <summary>
        /// Gets or Sets Provincia.
        /// </summary>
        public string Provincia { get; set; }

        /// <summary>
        /// Gets or Sets Población.
        /// </summary>
        public string Poblacion { get; set; }

        /// <summary>
        /// Gets or Sets Teléfono.
        /// </summary>
        public string Telefono { get; set; }

        /// <summary>
        /// Gets or Sets TeléfonoPúblico.
        /// </summary>
        public int TelefonoPublico { get; set; }

        /// <summary>
        /// Gets or Sets CP.
        /// </summary>
        public string CP { get; set; }

        /// <summary>
        /// Gets or Sets Dirección.
        /// </summary>
        public string Direccion { get; set; }

        /// <summary>
        /// Gets or Sets Latitud.
        /// </summary>
        public double Latitud { get; set; }

        /// <summary>
        /// Gets or Sets Longitud.
        /// </summary>
        public double Longitud { get; set; }

        /// <summary>
        /// Gets or Sets IdClinica.
        /// </summary>
        public IEnumerable<EspecialidadesPorCentroModel> EspecialidadesPorCentros { get; set; }
    }
}