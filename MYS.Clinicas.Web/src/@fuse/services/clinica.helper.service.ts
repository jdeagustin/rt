import { Injectable} from '@angular/core';
import { ClinicaModel } from 'app/models/clinica.model'
import { Observable } from 'rxjs';

export interface Locale
{
    lang: string;
    data: Object;
}

@Injectable({
    providedIn: 'root'
})
export class ClinicaHelperService
{
    private _clinica: ClinicaModel;
    hayClinica: boolean = false;

    /**
     * Constructor
     *
     * 
     */

    constructor(
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Introducir una clinica para ser recogida mas tarde
     */
    setClinica(clinica:ClinicaModel) {
        this._clinica = clinica; 
        this.hayClinica = true; 
    }

    /**
     * Desde aqui los componentes podran referenciar la clinica
     */
    getClinica(){
        return this._clinica;
    }
    
    /**
     * Recoger datos sueltos de la clinica
     */
    getDato(property:string){
        console.log(this._clinica.nombre);
        let returnedProp:string;
        switch (property) {
            case "nombre":
                    returnedProp = this._clinica.nombre;
                    break; 
        }
        return returnedProp;
    }
}
