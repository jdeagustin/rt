import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { LoginService } from './login.service';
import { ClinicaHelperService } from '@fuse/services/clinica.helper.service';
import { ClinicaModel } from 'app/models/clinica.model';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;
    _name: string;
    public _rememberMe: boolean;
    private _clinica: ClinicaModel;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {userTraspassingService} _userTraspassingService
     * @param {LoginService} _LoginService
     * @param {Router} _Router
     * @param {ClinicaHelperService} _clinicaHelperService
     * @param {FuseProgressBarService} _fuseProgressBarService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _LoginService: LoginService,
        private _Router: Router,
        private _clinicaHelperService: ClinicaHelperService,
        private _fuseProgressBarService: FuseProgressBarService
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.loginForm = this._formBuilder.group({
            name   : ['', [Validators.required, Validators.minLength(1)]],
            password: ['', Validators.required]
        });

        if ( localStorage.getItem('id') ) {
            this._name = localStorage.getItem('id');
            this._rememberMe = true;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Login
     */
    login(form:NgForm) {
        this._fuseProgressBarService.show();

        // Aqui deberia ir el codigo para verificar la autenticacion.
        // EL id es recogido en la variable "from.value.name" o con el "ngModel=name"

        this._LoginService.authenticate("","").subscribe(
            data => {
                this._clinica = data;
                this._clinicaHelperService.setClinica(this._clinica);
                if (this._rememberMe) {
                    localStorage.setItem("id", form.value.name);
                } else if ((!this._rememberMe) && (form.value.name == localStorage.getItem('id'))) {
                    localStorage.removeItem("id");
                }
                this._Router.navigateByUrl('/profile');
            },
            error=> {
                alert("No se encuentra ninguna clinica con ese ID. Por favor introduce un ID valido");
                this._fuseProgressBarService.hide();
            });
    }
}
