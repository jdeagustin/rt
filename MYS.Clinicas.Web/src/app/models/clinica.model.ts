export class ClinicaModel {

    id: string;
    nombre: string;
    tipoCentro: string;
    nombreEspecialista: string;
    codigoProvincia: string;
    provincia: string;
    poblacion: string;
    telefono: string;
    cp: string;
    direccion: string;
    latitud: string;
    longitud: string;
    especialidades: string[];
    
}