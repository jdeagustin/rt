import { Component, ViewEncapsulation, OnInit} from '@angular/core';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';

import { ClinicaModel } from 'app/models/clinica.model';
import { ClinicaHelperService } from '@fuse/services/clinica.helper.service';
import { AseguradoModel } from 'app/models/asegurado.model';
import { ProfileService } from './profile.service';
import { error } from '@angular/compiler/src/util';


@Component({
    selector     : 'profile',
    templateUrl  : './profile.component.html',
    styleUrls    : ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileComponent implements OnInit
{
    _clinica:ClinicaModel = new ClinicaModel;
    _cliente:AseguradoModel = new AseguradoModel;
    found: boolean = false;

    /**
     * Constructor
     * @param {FuseConfigService} _fuseConfigService
     * @param {ProfileService} _profileService
     * @param {Router} _router
     * @param {ClinicaHelperService} _clinicaHelperService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _profileService: ProfileService,
        private _router: Router,
        private _clinicaHelperService: ClinicaHelperService,
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                style    : 'vertical-layout-1',
                width    : 'fullwidth',
                navbar   : {
                    hidden             : true,
                    position           : 'top',
                },
                toolbar  : {
                    background           : 'fuse-white-500',
                    customBackgroundColor: true,
                    hidden               : false,
                    position             : 'above'
                },
                footer   : {
                    background           : 'lime-600',
                    customBackgroundColor: true,
                    hidden               : false
                },
                sidepanel: {
                    hidden  : true
                }
            }
        };
    }

    /**
     * On init
     */
    ngOnInit () {
        this._clinica = this._clinicaHelperService.getClinica();
    }


    /**
     * Buscador de asegurado por DNI
     */
    search(value): void
    {
        this._profileService.getCliente(value.target.value).subscribe(
            data => {
                this._cliente = data;
                this.found = true;
            },
            error=> {
                alert("No se encuentra ningun asegurado con ese DNI");
                this.found = false;
            });
        if (this.found) {

            // Metodos para el uso de los datos.
            
        }
    }
}

